#include <iostream>
#include <fstream>
#include <string>

int main(int argc, char *argv[]){
    bool exit = false;
    if (argc < 2) {
        // report version
        std::cout << argv[0] << " says: Hello World!" << std::endl;
        return 1;
    }
    std::ifstream controller_input;
    std::string myLine;
    controller_input.open("/dev/input/js0");
    while(!exit){
        while(getline(controller_input, myLine))
        {
            // print the line on the standard output
            std::cout << myLine << std::endl;
        }
    }
    controller_input.close();
}